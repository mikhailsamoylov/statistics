import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class FileOutput implements Output {
    @Override
    public void write(List<String> lines) throws IOException {
        Files.write(Paths.get(Config.OUTPUT_PATH), lines);
    }
}
