public enum AggregationMode {
    HOUR("yyyy-MM-dd HH"), MINUTE("yyyy-MM-dd HH:mm");

    private String key;

    AggregationMode(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }
}
