import java.util.Arrays;

public enum ErrorType {
    ERROR("ERROR"), WARNING("WARN"), UNKNOWN("UNKNOWN");

    private String name;

    ErrorType(String name) {
        this.name = name;
    }

    public static ErrorType getTypeByName(String name) {
        return Arrays.stream(ErrorType.values())
                .filter(type -> type.name.equals(name))
                .findFirst()
                .orElse(UNKNOWN);
    }
}
