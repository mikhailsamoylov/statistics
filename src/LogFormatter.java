import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class LogFormatter {
    List<LogEntity> format(List<String> logLines) {
        List<LogEntity> entities = new LinkedList<>();
        Pattern p = Pattern.compile("(.+);(.+); (.+)");

        for (String line : logLines) {
            Matcher m = p.matcher(line);
            if (m.find()) {
                entities.add(getFilledEntity(m));
            }
        }

        return entities;
    }

    private LogEntity getFilledEntity(Matcher m) {
        LogEntity entity = new LogEntity();
        entity.dateTime = LocalDateTime.parse(m.group(1), DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS"));
        entity.type = ErrorType.getTypeByName(m.group(2));
        entity.description = m.group(3);

        return entity;
    }
}
