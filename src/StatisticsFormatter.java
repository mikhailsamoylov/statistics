import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

class StatisticsFormatter {
    List<String> format(List<StatisticsEntity> statisticsEntities) {
        return statisticsEntities.stream()
                .map(entity -> entity.start.format(DateTimeFormatter.ofPattern("yyyy-MM-dd, HH.mm")) + '-' +
                        entity.end.format(DateTimeFormatter.ofPattern("HH.mm")) +
                        " Количество ошибок: " + entity.count)
                .sorted()
                .collect(Collectors.toList());
    }
}
