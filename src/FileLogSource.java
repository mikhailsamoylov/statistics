import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class FileLogSource implements LogSource {

    @Override
    public List<String> read() throws IOException {
        return Files.list(Paths.get(Config.LOG_DIRECTORY))
                .filter(filename -> Files.isRegularFile(filename) && filename.toString().endsWith(".log"))
                .flatMap(path -> {
                    try {
                        return Files.lines(path);
                    } catch (IOException e) {
                        throw new RuntimeException(e.getMessage());
                    }
                })
                .collect(Collectors.toList());
    }
}
