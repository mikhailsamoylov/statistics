import java.time.LocalDateTime;

class StatisticsEntity {
    LocalDateTime start;
    LocalDateTime end;
    Long count;
}
