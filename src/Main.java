import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        final long start = System.currentTimeMillis();
        new FileOutput().write(
                new StatisticsFormatter().format(
                        new StatisticsProcessor().process(
                                new LogFormatter().format(
                                        new FileLogSource().read()
                                )
                        )
                )
        );
        System.out.println((System.currentTimeMillis() - start));
    }
}
