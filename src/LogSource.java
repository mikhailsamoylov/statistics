import java.io.IOException;
import java.util.List;

public interface LogSource {
    List<String> read() throws IOException;
}
