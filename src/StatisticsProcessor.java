import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

class StatisticsProcessor {
    List<StatisticsEntity> process(List<LogEntity> logs) {
        return fillEntities(collectStatistics(logs));
    }

    private Map<String, Long> collectStatistics(List<LogEntity> logs) {
        Map<String, Long> statisticsMap = new HashMap<>();
        for (LogEntity log : logs) {
            if (log.type.equals(ErrorType.ERROR)) {
                String key = getKey(log);
                statisticsMap.putIfAbsent(key, 0L);
                statisticsMap.computeIfPresent(key, (k, v) -> ++v);
            }
        }

        return statisticsMap;
    }

    private String getKey(LogEntity entity) {
        LocalDateTime dateTime = entity.dateTime;
        return dateTime.format(DateTimeFormatter.ofPattern(Config.AGGREGATION_MODE.getKey()));
    }

    private List<StatisticsEntity> fillEntities(Map<String, Long> statisticsMap) {
        List<StatisticsEntity> entities = new LinkedList<>();
        for (Map.Entry<String, Long> statisticsEntry : statisticsMap.entrySet()) {
            entities.add(fillEntity(statisticsEntry));
        }

        return entities;
    }

    private StatisticsEntity fillEntity(Map.Entry<String, Long> statisticsEntry) {
        StatisticsEntity entity = new StatisticsEntity();
        entity.start = LocalDateTime.parse(
                statisticsEntry.getKey(),
                DateTimeFormatter.ofPattern(Config.AGGREGATION_MODE.getKey())
        );
        switch (Config.AGGREGATION_MODE) {
            case HOUR:
                entity.end = entity.start.plusHours(1);
                break;
            case MINUTE:
                entity.end = entity.start.plusMinutes(1);
                break;
            default:
                throw new RuntimeException("Unsupported aggregation mode");
        }
        entity.count = statisticsEntry.getValue();

        return entity;
    }
}
