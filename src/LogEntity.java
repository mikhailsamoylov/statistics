import java.time.LocalDateTime;

class LogEntity {
    LocalDateTime dateTime;
    ErrorType type;
    String description;
}
