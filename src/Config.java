class Config {
    static String LOG_DIRECTORY = "logs";
    static String OUTPUT_PATH = "output/test.stat";
    static AggregationMode AGGREGATION_MODE = AggregationMode.MINUTE;
}
